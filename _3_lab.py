import csv

FILENAME = "coordinates.csv"

coordinates = [
['x: ', 31, 37, 29, 41, 46, -3, -25, 14, -4, 40] ,
['y: ', 3, -30, 26, 20, 22, -3, 7, -48, -36, -23]
]

with open(FILENAME, "w", encoding='utf-8', newline="") as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(coordinates)